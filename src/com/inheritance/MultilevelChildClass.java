package com.inheritance;

public class MultilevelChildClass extends MultilevelParent{
public  void property3() {
	System.out.println("this belons to child");
}
public static void main(String[] args) {
	MultilevelChildClass child =new MultilevelChildClass();
	child.property1();
	child.property2();
	child.property3();
	MultilevelParent parent =new MultilevelParent();
	parent.property1();
	parent.property2();
	GrandparentClass grand=new GrandparentClass();
	grand.property1();
}
}
