package com.collections;

import java.util.ArrayList;
import java.util.LinkedList;

public class Linkedlistwithdupliandnull {
	public static void main(String[] args) {
		LinkedList<String>list=new LinkedList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		System.out.println(list);
		list.add("b");//duplicate value
		System.out.println(list);
		list.add(null);//null value
		System.out.println(list);
	}

}
