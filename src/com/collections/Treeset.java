package com.collections;

import java.util.TreeSet;

public class Treeset {
	 public static void main(String[] args) {
	        TreeSet<Integer> tree = new TreeSet<>();
	        tree.add(2);
	        tree.add(5);
	        tree.add(6);
	        System.out.println("TreeSet: " + tree);

	        // Using the remove() method
	        boolean val = tree.remove(5);
	        System.out.println("Is 5 removed? " + val);

	        // Using the removeAll() method
	        boolean value2 = tree.removeAll(tree);
	        System.out.println("Are all elements removed? " + value2);
	 }
}
