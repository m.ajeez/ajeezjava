package com.sample;

public class Practise {
	
	int a=0;  //instance variable
	 static int b=5; //static variable
	public void sample() {
		int c=20; //local variable
		a=a+5;
		System.out.println(a);
		
		

	}
	public static void main(String[] args) {
		Practise p=new Practise();
		Practise p1=new Practise();
		//a=a+20; //we cannot call a instance variable without creating an object
		//System.out.println(a);
		b=b-5;
		System.out.println(b);//we can access static variable without using an object
		//c=c*2; //the scope of local variable is only in the method
		
		
	}
	

}
