package com.string;

public class StringDemo2 {
	
	public static void main(String[] args) {
		String str="Welcome";
		String str1="Welcome";
		String str2=new String("Welcome");
		String str3="java";
		boolean a = str.equals(str1);//true
		boolean b=str.equals(str2);//true
		boolean c=str.equals(str3);//false
		boolean d=str.equals(str2);//true
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);//equals cheak the content not the reference
	boolean a1=str==str1;
	boolean a2=str==str2;
	boolean a3=str==str3;
	boolean a4=str1==str2;
	System.out.println(a1);
	System.out.println(a2);
	System.out.println(a3);
	System.out.println(a4);
	}

}
