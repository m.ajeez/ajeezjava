package com.string;

public class StringConcepts {
	public static void main(String[] args) {
		String s1="Welcome";//stores in stirng constant pool
		char[] c= {'s','e','l','e','n','i','u','m'};
		String s2="Welcome";//string constant pool
		String s3=new String("Welcome");//store in heap memory
		String  s4=new String("Hello World");
		System.out.println(c);
		String s5=new String(c);////store in heap memory
		System.out.println(s1);
		
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		System.out.println(s5);
		
		
	}

}
